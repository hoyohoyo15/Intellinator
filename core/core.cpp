// ConsoleApplication1.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <string>
#include <iostream>
#include <locale>
#include <assert.h>

using namespace std;

class Neuron {
public:
	bool istFrage;
	wstring frage;
	wstring antwort;
	Neuron *jaZweig;
	Neuron *neinZweig;
};

void startup(){

		wstring welcome = L"Willkommen bei Intellinator \n ======= \n Ich werde nach und nach lernen, eine von dir ausgedachte Sache oder Person zu erraten";
		wcout << welcome << endl;
	}

Neuron* game(Neuron* aktuellesWissen) {
	wstring question = aktuellesWissen->frage;
	wstring answer;
	wstring retry;
	wcout << question << endl;
	wstring answerYes = aktuellesWissen->jaZweig->antwort;
	assert(aktuellesWissen != nullptr);
	if (aktuellesWissen->istFrage == true) {
		wcout << L"Wie ist deine Antwort?" << endl;
		getline(wcin, answer);
		if (answer == L"Ja") {
			wcout << L"Ist es " << answerYes << L"?" << endl;
			wstring newAnswer;
			getline(wcin, newAnswer);
			if (newAnswer == L"Ja") {
				return aktuellesWissen;
			}
			else if (newAnswer == L"Nein") {
				//Nutzer hat auf Ja Zweig zur L�sung Nein gesagt, also:
				// muss erweitern...
				Neuron *neueFrage = new(Neuron);

				// Frage einlesen
				wcout << L"Formuliere eine Frage zu deiner Antwort:" << endl;
				getline(wcin, question);
				neueFrage->frage = question;

				// Neues Objekt einlesen
				Neuron *neuesObjekt = new(Neuron);

				//Antwort definieren
				wcout << L"Mit welcher Antwort zur vorherigen Frage bist du hier hergekommen?" << endl;
				getline(wcin, answer);
				if (answer == L"Nein") {
					wstring questAnswer;
					wcout << L"Was w�re die Antwort gewesen?" << endl;
					getline(wcin, questAnswer);
					neuesObjekt->antwort = questAnswer;
					aktuellesWissen->jaZweig->frage = neueFrage;
					aktuellesWissen->jaZweig->istFrage = true;
					aktuellesWissen->jaZweig->neinZweig->antwort = neuesObjekt;
				}

				return aktuellesWissen;
			}
			else {
				// muss erweitern...
				Neuron * neueFrage = new(Neuron);

				// Frage einlesen
				wcout << L"Formuliere eine Frage zu deiner Antwort:" << endl;
				getline(wcin, question);
				neueFrage->frage = question;

				// Neues Objekt einlesen
				Neuron *neuesObjekt = new(Neuron);

				//Antwort definieren
				wcout << L"Mit welcher Antwort zur vorherigen Frage bist du hier hergekommen?" << endl;
				getline(wcin, answer);
				if (answer == L"Nein") {
					wstring questAnswer;
					wcout << L"Was w�re die Antwort gewesen?" << endl;
					getline(wcin, questAnswer);
					neuesObjekt->antwort = questAnswer;
					aktuellesWissen->neinZweig = neueFrage;
					aktuellesWissen->jaZweig = neuesObjekt;
				}
				else {
					return aktuellesWissen;
				};
				game(aktuellesWissen);
			}
		return aktuellesWissen;
		};
	};
};
Neuron* initBrain(){
Neuron *neuron01 = new(Neuron);
neuron01->istFrage = true;
neuron01->frage = L"Ist es ein Mensch?";
neuron01->antwort = L"";
neuron01->jaZweig = new(Neuron);
neuron01->jaZweig->istFrage = false;
neuron01->jaZweig->antwort = L"Ines";
neuron01->neinZweig = new(Neuron);
neuron01->neinZweig->istFrage = false;
neuron01->neinZweig->antwort = L"Baum";
return neuron01;
}

int main() {
	bool inGame = false;
	startup();
	Neuron *basisWissen = initBrain();
	do {
		basisWissen = game(basisWissen);
		game(basisWissen);
	} while (inGame == true);
}